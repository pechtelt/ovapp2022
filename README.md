# OV-app | Hogeschool Utrecht

Met de OV-app kun je je reis van te voren plannen in het openbaar vervoer, hierin kunnen je verschillende vervoersmiddelen selecteren, een bepaalde tijd en jij krijgt de route die het beste uitkomt voor jou.

## Git - Start
Om te beginnen haal je de code naar je eigen computer toe, zorg dat je een Terminal/CMD opent in een folder waar je zo direct het project wilt hebben en vervolgen gebruik typ je de volgende command:
```bash
git clone <git_HTTPSurl>
```
De HTTPS url kun je gemakkelijk vinden door in het project op 'Clone' te klikken, en daar de URL te kopieren.

## cleanCode
We willen graag allemaal leesbare code hebben en daarvoor maken we gebruik van zo genoemd camelCase. Je maakt hier dus gebruik van de volgende format: watVolledigeNaam bijvoorbeeld getFullName, het werkwoord doe je dus in kleine letters maar de rest begin je steeds met hoofdletter.

Daarnaast gebruiken wij bij methodes de volgende structuur: 
```java
// korte methodes
public void getFullName(){return FullName;}

// Lange methodes
public void getFullName(){ 
   fucnties1;
   fucnties2;
}

// ERROR - 1!
public void getFullName(){

return FullName;
}

// ERROR - 2!
public void getFullName()
{ 
   fucnties1;
   fucnties2;
}


```
Belangrijk is dus dat altijd de '{' direct achter functies komt.


## Git - Branchen
We programmeren met meerdere mensen in het project, iedereen werkt aan zijn stukje code. Om te voorkomen dat iedereen in dezelfde code zijn code pusht, maken we gebruik van een branch. In principe werk je dus in je eigen versie van de huidige code en push je deze versie ook. Je voorkomt hiermee dat meerdere mensen in dezelfde branch hun code pushen wat fouten kan opleveren. Hier ligt ik verschillende branch command toe:

Aanmaken van een branch:
```bash
git checkout -b <name_of_branch>
```
Lijst van branches zien (de groene is de huidige branch):
```bash
git branch
```
Wisselen van een branch:
```bash
git checkout <name_of_branch>
```

## Git - Lokaal
Wanneer je gebracht hebt staat alles nog OFFLINE, dus op je eigen device. Dit kan niemand via Git zien, voordeel hiervan is dat jij zelf makkelijk kan switchen naar de oorspronkelijke code of kan switchen naar verschillende branches die je hebt aangemaakt. Op zo een branch op te slaan om daarna gemakkelijk te switchen maak je gebruik van commit. Je vult de volgende command in:
```bash
git commit -a -m <name_of_commit>
```
Doordat je '-a' er tussen voegt pakt hij eigenlijk direct alles waar een wijziging op zit. Je kan het ook op deze manier doen:
```bash
git add .
git commit -m <name_of_commit>
```
'.' betekent alles, dus de gehele inhoud van de folder. Als je dit gedaan hebt kun je switchen van branches terwijl je wijzigen blijven bestaan. Zie het als een soort verschillende word documenten. Je hebt een kopie gemaakt van een word document (branch), deze gewijzigd (in je editor) en opgeslagen (commit). 

## Git - Online
Heb je je code helemaal getest en werkt deze naar wens? Dan kan je hem naar Git toe sturen zodat je hier straks een merge request aankan gaan maken. Dit doe je als volgt (// = uitleg vanuit mij):
```bash
// je voegt je bestanden toe:
git add .

// Nog geen Repository voor je branch gemaakt:
git push --set-upstream origin <active_branch_name>

// Al wel een Repository voor je branch gemaakt:
git push
```
Mocht er nu nog wat mis gaan, dan vertelt Git vaak zelf als wat het is, Git is namelijk erg slim! ;)

## Gitlab - Merge request aanmaken

Als je Git Online gepusht hebt kan je een merge request aanmaken, dit doe je als volgt:
1. [Ga naar onze pagina voor branches](https://gitlab.com/pechtelt/ovapp2022/-/branches)
2. Selecteer jouw branch en klik op 'Merge request'
3. Check bovenaan of je je request in de juiste merged
4. Geef een passende titel en voeg een beschrijving toe met wat je toegevoegd hebt
5. Klik op Merge request wanneer bovenstaand allemaal gedaan is

## Version
05-10-2022 | Patrick: Creating the .README
