module com.example.ovapp2022 {
    requires javafx.controls;
    requires javafx.fxml;

    opens com.example.ovapp2022 to javafx.fxml;
    exports com.example.ovapp2022;
}
