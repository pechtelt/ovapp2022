package com.example.ovapp2022;

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.stage.Window;
import javafx.util.converter.LocalTimeStringConverter;

import java.io.IOException;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ResourceBundle;

public class Controller {
    Data data = new Data();
    BusData busData = new BusData();
    PlaneData planeData = new PlaneData();
    TrainData trainData = new TrainData();
    TramData tramData = new TramData();
    Translate translator = new Translate();

    @FXML private TabPane parent;
    @FXML private TextArea travelInformation;
    @FXML private TextArea favoriteTravelInformation;
    @FXML private ComboBox<String> transportInput;
    @FXML private ComboBox<String> departureInput;
    @FXML private ComboBox<String> arrivalInput;
    @FXML private ListView<Trip> listView;
    @FXML private ListView<Trip> favoriteListView;
    @FXML private Spinner timeSpinner;
    @FXML private Tab reisplanner;
    @FXML private Tab favTrips;
    @FXML private Tab langTab;
    @FXML private Button planTrips;
    @FXML private Button addToFavTrips;
    @FXML private Button removeFavoriteTrip;
    @FXML private Label tripLabel;
    @FXML private Label tripListLabel;
    @FXML private Label tripInfoLabel;
    @FXML private Label tripInfoLabelTwo;
    @FXML private Label languageLabelNL;
    @FXML private Label languageLabelEN;
    @FXML private Label languageLabelFR;
    @FXML private Label optionLabel;
    @FXML private Label favTripLabel;
    @FXML private ImageView flagNL;
    @FXML private ImageView flagEN;
    @FXML private ImageView flagFR;
    @FXML private CheckBox darkMode;

    private ObservableList<Trip> favoriteTripList = FXCollections.observableArrayList();
    Alert a = new Alert(Alert.AlertType.ERROR);
    public String getKey1() {return departureInput.getValue(); }
    public String getKey2() {return arrivalInput.getValue(); }
    private static DateTimeFormatter formatter = DateTimeFormatter.ofPattern("HH:mm");


    SpinnerValueFactory value = new SpinnerValueFactory<LocalTime>() {

        {
            setConverter(new LocalTimeStringConverter(formatter,null));
        }

        @Override
        public void decrement(int steps) {
            if (getValue() == null)
                setValue(LocalTime.now());
            else {
                LocalTime time = (LocalTime) getValue();
                setValue(time.minusMinutes(steps));
            }
        }

        @Override
        public void increment(int steps) {
            if (this.getValue() == null)
                setValue(LocalTime.now());
            else {
                LocalTime time = (LocalTime) getValue();
                setValue(time.plusMinutes(steps));
            }
        }
    };

    public void onPlanMyTripClick() {
        listView.getItems().clear();

        if (getKey1() == null && getKey2() == null) {
            a.setTitle("Error!");
            a.setHeaderText(translator.translate("cantShowRouteError"));
            a.setContentText(translator.translate("noDepAndArr"));
            a.show();
        } else if (getKey1() == null) {
            a.setTitle("Error!");
            a.setHeaderText(translator.translate("cantShowRouteError"));
            a.setContentText(translator.translate("noDepLocation"));
            a.show();
        } else if (getKey2() == null) {
            a.setTitle("Error!");
            a.setHeaderText(translator.translate("cantShowRouteError"));
            a.setContentText(translator.translate("noArrLocation"));
            a.show();
        } else if (getKey1() == getKey2()) {
            a.setTitle("Error!");
            a.setHeaderText(translator.translate("cantShowRouteError"));
            a.setContentText(translator.translate("sameLocation"));
            a.show();
        } else {

            ObservableList<Trip> tripList = FXCollections.observableArrayList(data.getTrips(getKey1(), getKey2(), LocalTime.parse(timeSpinner.getValue().toString())));
            listView.setItems(tripList);
        }
    }

    public void setTransportInput(){
        if (transportInput.getValue() == "Bus") {
            data = busData;
        }
        else if (transportInput.getValue() == translator.translate("train")) {
            data = trainData;
        }
        else if (transportInput.getValue() == "Tram") {
            data = tramData;
        }
        else if (transportInput.getValue() == translator.translate("plane")) {
            data = planeData;
        }
        else {
            System.out.println("ERROR!");
        }
        ObservableList<String> locations = FXCollections.observableArrayList(data.getAllLocations());
        departureInput.setItems(locations);
        arrivalInput.setItems(locations);
    }

    public void initialize(){
        ObservableList<String> locations = FXCollections.observableArrayList(data.getAllLocations());
        departureInput.setItems(locations);
        arrivalInput.setItems(locations);


        timeSpinner.setValueFactory(value);
        timeSpinner.setEditable(true);
        timeSpinner.getValueFactory().setValue(LocalTime.now());

        // Sets all transport methods in the combobox
        data.setAllTransportMethods();
        ObservableList<String> transportMethods = FXCollections.observableArrayList(data.getAllTransportMethods());
        transportInput.setItems(transportMethods);
    }

    public void switchScene(String fxml) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource(fxml));
        Scene scene = new Scene(fxmlLoader.load(), 1250,666);
        stage.setScene(scene);
        stage.show();
    }

    private boolean isLightMode = true;
    public void setColorModus() {
        isLightMode = !isLightMode;
        if (isLightMode){
            setLightMode();
        }
        else {
            setDarkMode();
        }
    }

    private void setLightMode(){
        parent.getStylesheets().remove("darkmodus.css");
        parent.getStylesheets().add("lightmodus.css");
    }

    private void setDarkMode(){
        parent.getStylesheets().remove("lightmodus.css");
        parent.getStylesheets().add("darkmodus.css");
    }

    public void listViewClick(){
        Trip selectedItem = listView.getSelectionModel().getSelectedItem();
        travelInformation.setText(selectedItem.writeTravelInformation());
    }

    public void favoriteListViewClick(){
        Trip selectedItem = favoriteListView.getSelectionModel().getSelectedItem();
        favoriteTravelInformation.setText(selectedItem.writeTravelInformation());
    }

    public void addTrip(){
        Trip selectedItem = listView.getSelectionModel().getSelectedItem();
        favoriteTripList.add(selectedItem);
        favoriteListView.setItems(favoriteTripList);

    }

    public void removeTrip(){
        Trip selectedItem = favoriteListView.getSelectionModel().getSelectedItem();
        favoriteTripList.remove(selectedItem);
    }

    public void repopulate(MouseEvent event){

        String langID = event.getPickResult().getIntersectedNode().getId();
        if(langID.equals("flagEN") || langID.equals("languageLabelEN")){
            translator.setLocale("en", "US");
        }
        if(langID.equals("flagNL") || langID.equals("languageLabelNL")){
            translator.setLocale("nl", "NL");
        }
        if(langID.equals("flagFR") || langID.equals("languageLabelFR")){
            translator.setLocale("fr", "FR");
        }

        ///////////////////////////////Tabs/////////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////
        reisplanner.setText(translator.translate("planner"));
        favTrips.setText(translator.translate("favTrips"));
        langTab.setText((translator.translate("langTabText")));


        ///////////////////////Combobox Details/////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        transportInput.getItems().clear();
        if(langID.equals("flagNL")){
            data.repopulateTransportMethodArrList("nl", "NL");
        }
        if(langID.equals("flagEN")){
            data.repopulateTransportMethodArrList("en", "US");
        }
        if(langID.equals("flagFR")){
            data.repopulateTransportMethodArrList("fr", "FR");
        }
        ObservableList<String> transportMethods = FXCollections.observableArrayList(data.getAllTransportMethods());
        transportInput.setItems(transportMethods);


        ///////////////////////Combobox Prompts/////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////


        transportInput.setPromptText(translator.translate("transportMethod"));
        departureInput.setPromptText(translator.translate("depInput"));
        arrivalInput.setPromptText(translator.translate("arrInput"));


        ////////////////////////////////Buttons/////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        planTrips.setText(translator.translate("planTrips"));
        addToFavTrips.setText(translator.translate("addFavTrips"));
        removeFavoriteTrip.setText(translator.translate("remFavTrips"));


        /////////////////////////////////Labels/////////////////////////////////////
        ////////////////////////////////////////////////////////////////////////////

        tripLabel.setText(translator.translate("tripLabelText"));
        tripListLabel.setText(translator.translate("tripListLabelText"));
        tripInfoLabel.setText(translator.translate("tripInfoLabelText"));
        tripInfoLabelTwo.setText(translator.translate("tripInfoLabelText"));
        favTripLabel.setText(translator.translate("favTrips"));
        optionLabel.setText(translator.translate("options"));
        darkMode.setText(translator.translate("darkModeText"));

    }

}