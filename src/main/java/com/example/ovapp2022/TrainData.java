package com.example.ovapp2022;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class TrainData extends Data
{

    public TrainData()
    {
        var location = new Location("Amsterdam");
        locationMap.put(location.getName(), location);

        location = new Location("Arnhem");
        locationMap.put(location.getName(), location);

        location = new Location("Assen");
        locationMap.put(location.getName(), location);

        location = new Location("Den Haag");
        locationMap.put(location.getName(), location);

        location = new Location("Groningen");
        locationMap.put(location.getName(), location);

        location = new Location("Haarlem");
        locationMap.put(location.getName(), location);

        location = new Location("Leeuwarden");
        locationMap.put(location.getName(), location);

        location = new Location("Lelystad");
        locationMap.put(location.getName(), location);

        location = new Location("Maastricht");
        locationMap.put(location.getName(), location);

        location = new Location("Middelburg");
        locationMap.put(location.getName(), location);
        location = new Location("'s-Hertogenbosch");
        locationMap.put(location.getName(), location);
        location = new Location("Utrecht");
        locationMap.put(location.getName(), location);
        location = new Location("Zwolle");
        locationMap.put(location.getName(), location);
        //// van en naar Amsterdam

        // === Routes Amsterdam ===> Arnhem ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Routes Arnhem ===> Amsterdam ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        // === Route Amsterdam ===> Assen ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Assen ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }

        // === Route Amsterdam ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Haarlem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Den Haag ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Haarlem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        // === Route Amsterdam ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Groningen ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Leeuwarden"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour , 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Haarlem ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Den Haag"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Den Haag"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Amsterdam ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 45));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Amsterdam ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 45));
            routeMap.put(route.getKey(), route);
        }


        // === Route Amsterdam ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Amsterdam"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Amsterdam  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Amsterdam"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Arnhem////////////////////////////////

        // === Route Arnhem ===> Assen ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Assen ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Den Haag ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Groningen ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour , 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Haarlem ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Arnhem ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 45));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Arnhem ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 30);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 45));
            routeMap.put(route.getKey(), route);
        }


        // === Route Arnhem ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Arnhem"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Arnhem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Arnhem"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Assen////////////////////////////////

        // === Route Assen ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Den Haag ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Groningen ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour , 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Haarlem ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Groningen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Assen ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Assen ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Assen ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Assen"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Assen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Assen"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Den Haag////////////////////////////////

        // === Route Den Haag ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Groningen ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour , 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Haarlem ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Den Haag ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Den Haag ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Den Haag ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Den Haag"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Den Haag  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Den Haag"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Groningen////////////////////////////////

        // === Route Groningen ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 59), LocalTime.of(hour, 55));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 13));
            routeMap.put(route.getKey(), route);
        }
        // === Route Haarlem ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 34), LocalTime.of(hour, 33));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 50));
            routeMap.put(route.getKey(), route);
        }


        // === Route Groningen ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Groningen ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Groningen ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


       //  === Route Groningen ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Groningen ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Groningen ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Groningen ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Groningen ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Groningen"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Groningen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Assen"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Groningen"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Haarlem////////////////////////////////

        // === Route Haarlem ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Leeuwarden ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Haarlem ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Haarlem ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Haarlem ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Den Haag"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Den Haag"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Haarlem ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Haarlem ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Haarlem ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Haarlem ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Haarlem"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Haarlem  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Haarlem"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Leeuwarden////////////////////////////////

        // === Route Leeuwarden ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Lelystad ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Leeuwarden ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 51), LocalTime.of(hour, 54));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 59));
            routeMap.put(route.getKey(), route);
        }


        // === Route Leeuwarden ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Leeuwarden ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Leeuwarden ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Leeuwarden ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Zwolle"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Leeuwarden ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leeuwarden"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Leeuwarden  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Leeuwarden"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Lelystad////////////////////////////////

        // === Route Lelystad ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Maastricht ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }


        // === Route Lelystad ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("Amsterdam"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Lelystad ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Lelystad ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 5));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Lelystad ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Lelystad ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Lelystad"), departure);
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Lelystad  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addEndPoint(locationMap.get("Lelystad"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Maastricht////////////////////////////////

        // === Route Maastricht ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Middelburg ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Maastricht ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Maastricht ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Maastricht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }


        // === Route Maastricht ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maastricht"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Maastricht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Middelburg////////////////////////////////

        // === Route Middelburg ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        // === Route 's-Hertogenbosch ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Middelburg ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Middelburg ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Middelburg"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route Middelburg ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Middelburg"), departure);
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Middelburg  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Arnhem"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addStopOver(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33), LocalTime.of(hour, 36));
            route.addEndPoint(locationMap.get("Maastricht"), LocalTime.of(hour, 51));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar 's-Hertogenbosch////////////////////////////////

        /// === Routes 's-Hertogenbosch ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> 's-Hertogenbosch ========
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 15));
            routeMap.put(route.getKey(), route);
        }


        // === Route 's-Hertogenbosch ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("'s-Hertogenbosch"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> 's-Hertogenbosch  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("'s-Hertogenbosch"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }

        ///////////////////////////////Van en naar Utrecht////////////////////////////////

        // === Route Utrecht ===> Zwolle  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Zwolle"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
        // === Route Zwolle ===> Utrecht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Zwolle"), departure);
            route.addStopOver(locationMap.get("Lelystad"), LocalTime.of(hour, 15), LocalTime.of(hour, 18));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 33));
            routeMap.put(route.getKey(), route);
        }
    }
}
