package com.example.ovapp2022;

import java.util.Locale;
import java.util.ResourceBundle;

public class Translate {
    private Locale locale;
    private ResourceBundle bundle;

    Translate(){
        locale = new Locale("nl", "NL");
        bundle = ResourceBundle.getBundle("bundle", locale);
    }

    public void setLocale(String language, String country){
        locale = new Locale(language, country);
        bundle = ResourceBundle.getBundle("bundle", locale);
    }

    public Locale getLocale(){
        return locale;
    }

    public String translate(String key){
        return bundle.getString(key);
    }

}
