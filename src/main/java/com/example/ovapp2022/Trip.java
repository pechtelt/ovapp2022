package com.example.ovapp2022;

import java.time.Duration;
import java.time.LocalTime;
import static java.time.temporal.ChronoUnit.MINUTES;

// One trip
public class Trip{
    private final Route route;
    private final LocalTime departure;
    private final LocalTime arrival;
    private final Location locationA;
    private final Location locationB;

    public Trip(Route route, Location locationA, Location locationB){
        this.route = route;
        this.locationA = locationA;
        this.locationB = locationB;
        this.departure = route.getDepartureTime(locationA);
        this.arrival   = route.getArrivalTime(locationB);
    }

    public LocalTime getDeparture() {
        return departure;
    }

    @Override
    public String toString() {
        return  locationA.getName() + " -> " + locationB.getName() + " (" + departure + " -> " + arrival + ")";
    }

    public String writeTravelInformation(){
        var duration = MINUTES.between(departure, arrival);
        String travelInformation = " Reisinformatie: \n Vertrektijd (" + departure + ") Aankomsttijd (" + arrival + ")\n Route: (" + route.getRouteString() +  ")\n De reis duurt: (" + duration + " min" + ")";
        return travelInformation;
    }


    public String writeTrip(String departureLocation, String arrivalLocation){
        // {LocationA} (vertrektijd: xx.xx) naar {LocationB} (aankomsttijd)
        String trip = departureLocation + " vertrektijd (" + departure + ") naar " +  arrivalLocation + " vertrektijd (" + arrival + ")";
        return trip;
    }
}
