package com.example.ovapp2022;

import java.util.*;
public class Trips
{
    private final ArrayList<Trip> trips = new ArrayList<>();
    public ArrayList<Trip> getTrips() { return trips; }

    public void addTrip(Trip trip) { trips.add(trip); }

}