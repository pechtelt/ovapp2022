package com.example.ovapp2022;

import java.time.LocalTime;
import java.util.HashMap;
import java.util.SortedMap;
import java.util.TreeMap;

public class BusData extends Data
{
    public BusData()
    {
        var location = new Location("Leidsche Rijn");
        locationMap.put(location.getName(), location);

        location = new Location("Maarssen");
        locationMap.put(location.getName(), location);

        location = new Location("Nieuwegein");
        locationMap.put(location.getName(), location);

        location = new Location("Overvecht");
        locationMap.put(location.getName(), location);

        location = new Location("Utrecht");
        locationMap.put(location.getName(), location);

        ////////////////////// van en naar Leidsche Rijn//////////////////////

        // === Routes Leidsche Rijn ===> Maarsen ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leidsche Rijn"), departure);
            route.addEndPoint(locationMap.get("Maarssen"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        // === Route Leidsche Rijn ===> Nieuwegein ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leidsche Rijn"), departure);
            route.addEndPoint(locationMap.get("Nieuwegein"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }
        // === Route Nieuwegein ===> Leidsche Rijn  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein"), departure);
            route.addEndPoint(locationMap.get("Leidsche Rijn"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        // === Route Leidsche Rijn ===> Overvecht  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leidsche Rijn"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Overvecht"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        // === Route Overvecht ===> Leidsche Rijn  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Overvecht"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Leidsche Rijn"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        // === Route Leidsche Rijn ===> Utrecht  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Leidsche Rijn"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        // === Route Utrecht ===> Leidsche Rijn  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Leidsche Rijn"), LocalTime.of(hour , 20));
            routeMap.put(route.getKey(), route);
        }

        ////////////////van en naar Maarssen///////////////////////////////////////////////////////////

        // === Route Maarssen ===> Nieuwegein  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maarssen"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Nieuwegein"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        // === Route Nieuwegein ===> Maarssen  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Maarssen"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        // === Route Maarssen ===> Overvecht  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maarssen"), departure);
            route.addEndPoint(locationMap.get("Overvecht"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        // === Route Overvecht ===> Maarssen  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Overvecht"), departure);
            route.addEndPoint(locationMap.get("Maarssen"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        // === Route Maarssen ===> Utrecht  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Maarssen"), departure);
            route.addStopOver(locationMap.get("Leidsche Rijn"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        // === Route Utrecht ===> Maarssen  ===
        for (int hour = 7; hour <= 19; hour += 2) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addStopOver(locationMap.get("Leidsche Rijn"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Maarssen"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }

        ////////////////////van en naar Nieuwegein//////////////////////////////////

        // === Route Nieuwegein ===> Overvecht  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Overvecht"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }
        // === Route Overvecht ===> Nieuwegein  ===
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Overvecht"), departure);
            route.addStopOver(locationMap.get("Utrecht"), LocalTime.of(hour, 20), LocalTime.of(hour, 22));
            route.addEndPoint(locationMap.get("Nieuwegein"), LocalTime.of(hour, 42));
            routeMap.put(route.getKey(), route);
        }


        /// === Routes Nieuwegein ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Nieuwegein"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht ===> Nieuwegein ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Nieuwegein"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }

        //////////////van en naar Overvecht///////////////////

        /// === Routes Overvecht ===> Utrecht ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Overvecht"), departure);
            route.addEndPoint(locationMap.get("Utrecht"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }
        /// === Routes Utrecht ===> Overvecht ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht"), departure);
            route.addEndPoint(locationMap.get("Overvecht"), LocalTime.of(hour, 20));
            routeMap.put(route.getKey(), route);
        }
    }
}
