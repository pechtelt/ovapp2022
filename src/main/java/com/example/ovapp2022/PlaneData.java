package com.example.ovapp2022;

import java.time.LocalTime;

public class PlaneData extends Data {

    PlaneData(){
        /// === Locations airports ========

        var location = new Location("Airport Schiphol");
        locationMap.put(location.getName(), location);

        location = new Location("Malta International Airport");
        locationMap.put(location.getName(), location);

        location = new Location("Barcelona International Airport");
        locationMap.put(location.getName(), location);

        location = new Location("Athene Airport");
        locationMap.put(location.getName(), location);

        location = new Location("Route Malta International Airport");
        locationMap.put(location.getName(), location);

        ////////////////////////////////////////////////////////////

        /// === Route Airport Schiphol - Malta International Airport ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Airport Schiphol"), departure);
            route.addEndPoint(locationMap.get("Malta International Airport"), LocalTime.of(hour + 5, 13));
            routeMap.put(route.getKey(), route);
        }

        /// === Route Airport Schiphol - Barcelona International Airport ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Airport Schiphol"), departure);
            route.addEndPoint(locationMap.get("Barcelona International Airport"), LocalTime.of(hour + 4, 29));
            routeMap.put(route.getKey(), route);
        }

        /// === Route Airport Schiphol - Athene Airport ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Airport Schiphol"), departure);
            route.addEndPoint(locationMap.get("Athene Airport"), LocalTime.of(hour + 5, 38));
            routeMap.put(route.getKey(), route);
        }

        /// === Route Malta International Airport - Airport Schiphol ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Route Malta International Airport"), departure);
            route.addEndPoint(locationMap.get("Airport Schiphol"), LocalTime.of(hour + 5, 19));
            routeMap.put(route.getKey(), route);
        }

        /// === Barcelona International Airport - Airport Schiphol ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Barcelona International Airport"), departure);
            route.addEndPoint(locationMap.get("Airport Schiphol"), LocalTime.of(hour + 4, 54));
            routeMap.put(route.getKey(), route);
        }

        /// === Athene Airport - Airport Schiphol ========
        for (int hour = 0; hour <= 23; hour += 8) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Athene Airport"), departure);
            route.addEndPoint(locationMap.get("Airport Schiphol"), LocalTime.of(hour + 5, 12));
            routeMap.put(route.getKey(), route);
        }

    }

}
