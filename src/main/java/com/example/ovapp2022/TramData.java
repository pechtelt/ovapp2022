package com.example.ovapp2022;

import java.time.LocalTime;

public class TramData extends Data{

    TramData(){
        /// === Locations Tram Utrecht ========

        var location = new Location("Utrecht CS");
        locationMap.put(location.getName(), location);

        location = new Location("Graadt van Roggenweg");
        locationMap.put(location.getName(), location);

        location = new Location("24 Oktoberplein-Zuid");
        locationMap.put(location.getName(), location);

        location = new Location("Winkelcentrum Kanaleneiland");
        locationMap.put(location.getName(), location);

        location = new Location("Vasco da Gamalaan");
        locationMap.put(location.getName(), location);

        location = new Location("Kanaleneiland-Zuid");
        locationMap.put(location.getName(), location);

        location = new Location("P+R Westraven");
        locationMap.put(location.getName(), location);


        ////////////////////////////////////////////////////////////

        /// === Routes Utrecht CS ... P+R Westraven ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("Utrecht CS"), departure);
            route.addStopOver(locationMap.get("Graadt van Roggenweg"), LocalTime.of(hour, 1), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("24 Oktoberplein-Zuid"), LocalTime.of(hour, 3), LocalTime.of(hour, 3));
            route.addStopOver(locationMap.get("Winkelcentrum Kanaleneiland"), LocalTime.of(hour, 5), LocalTime.of(hour, 5));
            route.addStopOver(locationMap.get("Vasco da Gamalaan"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Kanaleneiland-Zuid"), LocalTime.of(hour, 8), LocalTime.of(hour, 8));
            route.addEndPoint(locationMap.get("P+R Westraven"), LocalTime.of(hour, 11));
            routeMap.put(route.getKey(), route);
        }

        /// === Routes Utrecht CS ... P+R Westraven ========
        for (int hour = 7; hour <= 19; hour += 1) {
            var departure = LocalTime.of(hour, 0);
            var route = new Route(locationMap.get("P+R Westraven"), departure);
            route.addStopOver(locationMap.get("Kanaleneiland-Zuid"), LocalTime.of(hour, 1), LocalTime.of(hour, 1));
            route.addStopOver(locationMap.get("Vasco da Gamalaan"), LocalTime.of(hour, 3), LocalTime.of(hour, 3));
            route.addStopOver(locationMap.get("Winkelcentrum Kanaleneiland"), LocalTime.of(hour, 5), LocalTime.of(hour, 5));
            route.addStopOver(locationMap.get("24 Oktoberplein-Zuid"), LocalTime.of(hour, 6), LocalTime.of(hour, 6));
            route.addStopOver(locationMap.get("Graadt van Roggenweg"), LocalTime.of(hour, 8), LocalTime.of(hour, 8));
            route.addEndPoint(locationMap.get("Utrecht CS"), LocalTime.of(hour, 11));
            routeMap.put(route.getKey(), route);
        }


    }

}
