package com.example.ovapp2022;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalTime;

public class Application extends javafx.application.Application {
    @Override
    public void start(Stage primaryStage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("homeTabs.fxml"));
        primaryStage.setTitle("OV APP");
        primaryStage.setScene(new Scene(root,1000,620));
        primaryStage.show();

        System.out.println("Runtime: " + LocalTime.now());
    }


    public static void main(String[] args) {
        launch();
    }
}