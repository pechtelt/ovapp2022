package com.example.ovapp2022;

import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

public class FavoriteTripsController {
    public void switchScene(String fxml) throws IOException {
        Stage stage = new Stage();
        FXMLLoader fxmlLoader = new FXMLLoader(Application.class.getResource(fxml));
        Scene scene = new Scene(fxmlLoader.load(), 1250,666);

        stage.setScene(scene);
        stage.show();
    }

    public void onTripPlannerClick() throws IOException{
        switchScene("home.fxml");
    }

    public void onFavoriteTripClick() throws IOException{
        switchScene("favoritetrips.fxml");
    }
}
